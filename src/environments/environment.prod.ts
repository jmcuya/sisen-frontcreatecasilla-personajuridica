export const environment = {
  production: true,
  serviceUrl: 'https://servicioregistro.itgcloud365.com',
  snackbarDuration: 3000,
  KeycodeCaptcha: '6Lcj30ohAAAAAHA4Uuvqo1kDDKF3OcJQj_rrjpV0'

};

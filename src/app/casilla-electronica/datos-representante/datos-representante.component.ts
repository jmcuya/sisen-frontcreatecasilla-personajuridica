import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild, ViewEncapsulation,  Renderer2} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {CasillaService} from "../../core/services/casilla.service";
import {Cargo, Condicion_Persona_Natural,TipoDocumento,TipoDocumento_DNI, TipoDocumento_CE} from "../../core/dto/documento";
import {firstValueFrom, Subscription} from "rxjs";
import {ValidarCorreoService} from "../../core/services/validar-correo.service";
import {Departamento, Distrito, Provincia} from "../../core/dto/ubigeo.dto";
import {UbigeoService} from "../../core/services/ubigeo.service";
import { requestGlobal, RequestRepresentante } from 'src/app/core/dto/request';
import { FileUploadControl, FileUploadValidators } from '@iplab/ngx-file-upload';
import {ObtenerDatosPersonaDniDto, PersonaNaturalDni} from "../../core/dto/personaNaturalDni";
import {PersonaNaturalService} from "../../core/services/persona-natural.service";
import {AlertDialogComponent} from "../alert-dialog/alert-dialog.component";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {RECAPTCHA_V3_SITE_KEY, RecaptchaV3Module,ReCaptchaV3Service,} from 'ng-recaptcha';
import { SharedDialogComponent } from '../shared/shared-dialog/shared-dialog.component';
import { requestValidateRepresentative } from 'src/app/core/dto/personaJuridica';
import { PersonaJuridicaService } from 'src/app/core/services/persona-juridica.service';

@Component({
  selector: 'app-datos-representante',
  templateUrl: './datos-representante.component.html',
  styleUrls: ['./datos-representante.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class DatosRepresentanteComponent implements OnInit {
  @ViewChild('fileUpload', { static: false }) fileUpload !: ElementRef;
  @Output() completedStep = new EventEmitter<any>()
  @Output() previousStep = new EventEmitter<any>()
  formGroup!: FormGroup;
  public animation: boolean = false;
  public multiple: boolean = false;
  validateRequestRepresentative : requestValidateRepresentative = new requestValidateRepresentative ();
  observableRequestSubscription!: Subscription;
  requestSave: requestGlobal = new requestGlobal();
  requestRepresentante : RequestRepresentante = new RequestRepresentante();
  maxsize_ = 10485760;
  tipoDocumentoAdjuntoList: Array<TipoDocumento> = []
  tipoDocumentoList: Array<TipoDocumento> = []
  departamentoList: Array<Departamento> = []
  provinciaList: Array<Provincia> = []
  distritoList: Array<Distrito> = []
  cargoList: Array<Cargo> = []
  codigoEnviado = false;
  maxlength : number = 8;
  minlength : number = 8;
  esIos: boolean = false;
  blockInput : boolean = true;
  TOkenCaptcha: string = '';
  cont = 0;
  public loading: boolean = false;
  public buscando: boolean = false;
  personaNaturalDni: PersonaNaturalDni | null = null;
  numeroDniValido: Boolean | undefined = undefined;
  oneClick = false;
  todaydate : Date = new Date( new Date().setFullYear(new Date().getFullYear() - 18));
  
  constructor(
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    private casillaService: CasillaService,
    private ubigeoService: UbigeoService,
    private renderer: Renderer2,
    private personaNaturalService: PersonaNaturalService,
    private reCaptchaV3Service: ReCaptchaV3Service,
    private correoService : ValidarCorreoService,
    private validarCorreoService: ValidarCorreoService,
    private personaJuridicaService : PersonaJuridicaService
  ) {

    this.observableRequestSubscription = casillaService.casilla$.subscribe(
      (requestSave: requestGlobal) => {
        this.requestSave = requestSave;
        //if (requestSave) this.companyId = requestSave;
      }
    );
  }


  validatorFile(){
    const Image = this.formGroup.controls['files'].value[0];

    if(Image.size >= this.maxsize_){
      this.dialog.open(AlertDialogComponent, {
        disableClose: true,
        hasBackdrop: true,
        data: {cabecera : '!Advertencia!' ,messages: ['El peso del archivo adjunto no debe superar los 10MB']}
      })
      this.filesControl.setValue([]);

    }
  }

  async ngOnInit() {
    this.createForm();
    this.tipoDocumentoAdjuntoList = await firstValueFrom(this.casillaService.getTipoDocumentoAdjuntoList())
    this.tipoDocumentoList = await firstValueFrom(this.casillaService.getTipoDocumentoList(Condicion_Persona_Natural))
    this.departamentoList = await firstValueFrom(this.ubigeoService.getDepartamentoList())
    this.cargoList = await firstValueFrom(this.casillaService.getCargoList())
  }

  // handleArchivoAgregado(event: any) {
  //   console.log(event)
  //   this.formGroup.get('file')?.setValue(event)
  // }

  regresar() {
    this.previousStep.emit()
  }

  createForm(value ="",valuetipo = "", dataEspecifique = ""){
    this.formGroup = this.formBuilder.group({
      tipoDocumentoAdjunto: [valuetipo, Validators.required],
      tipoDocumentoAdjuntoNombre: [{ value: dataEspecifique, disabled: true }],
      //especifiqueDoc : FormControl = new FormControl({ value: '', disabled: true });
      files: this.filesControl,
      tipoDocumento: [value, Validators.required],
      cargo: [''],
      numeroDocumento: ['', [Validators.required,Validators.pattern('^[0-9]*$')]],
      apellidoPaterno: [''],
      apellidoMaterno: [''],
      nombres: ['', [Validators.required ]],
     // razonSocial: ['', Validators.required],
      //fechaNacimento: ['', Validators.required],
      //digitoVerificacion: ['',  Validators.required ],
      correoElectronico: ['',[ Validators.required, Validators.email]],
      numeroCelular: ['', Validators.required],
      numeroTelefono: [''],
     // ciudadTelefono: ['', Validators.required],
      //telefono: ['', Validators.required],
      departamento: ['', Validators.required],
      provincia: ['', Validators.required],
      distrito: ['', Validators.required],
      domicilioFisico: ['', Validators.required],
      //cargoNombre: ['', Validators.required],
      // paginaWeb: ['', Validators.required],
      validateEmail : [false, Validators.required],      
      recaptchaReactive: ['']
    })

    this.tipoDocumentoAdjuntoChangue();

  }

  tipoDocumentoAdjuntoChangue(){

    
    var tipoDocAdjCod = this.formGroup.get('tipoDocumentoAdjunto')?.value; 
    
    console.log("tipodocumento --->", tipoDocAdjCod);

    if(tipoDocAdjCod === '4'){
      this.formGroup.get('tipoDocumentoAdjuntoNombre')?.enable();
      this.formGroup.get('tipoDocumentoAdjuntoNombre')?.setValidators([Validators.required]);
      this.formGroup.get('tipoDocumentoAdjuntoNombre')?.updateValueAndValidity();
      this.formGroup.get('tipoDocumentoAdjuntoNombre')?.markAllAsTouched();
    
    }else{
      this.formGroup.get('tipoDocumentoAdjuntoNombre')?.disable();
      this.formGroup.get('tipoDocumentoAdjuntoNombre')?.setValue("");
      this.formGroup.get('tipoDocumentoAdjuntoNombre')?.clearValidators();
      this.formGroup.get('tipoDocumentoAdjuntoNombre')?.updateValueAndValidity();
    }
  }

  
  desactivarInputsInit(){
    this.formGroup.get('numeroDocumento')?.disable();
   // this.formGroup.get('razonSocial')?.disable();
    //this.formGroup.get('digitoVerificacion')?.disable();
    this.formGroup.get('correoElectronico')?.disable();
    this.formGroup.get('numeroCelular')?.disable();
    this.formGroup.get('apellidoPaterno')?.disable();
    this.formGroup.get('apellidoMaterno')?.disable();
    this.formGroup.get('nombres')?.disable();
    //this.formGroup.get('fechaNacimento')?.disable();    
    this.formGroup.get('numeroTelefono')?.disable();
   // this.formGroup.get('ciudadTelefono')?.disable();
    //this.formGroup.get('telefono')?.disable();
    this.formGroup.get('departamento')?.disable();
    this.formGroup.get('provincia')?.disable();
    this.formGroup.get('distrito')?.disable();
    this.formGroup.get('domicilioFisico')?.disable();
    // this.formGroup.get('paginaWeb')?.disable();

  }

  activarInputs(){
    this.formGroup.get('numeroDocumento')?.enable();
    //this.formGroup.get('razonSocial')?.enable();
    //this.formGroup.get('digitoVerificacion')?.enable();
    this.formGroup.get('correoElectronico')?.enable();
    this.formGroup.get('numeroCelular')?.enable();
    this.formGroup.get('apellidoPaterno')?.enable();
    this.formGroup.get('apellidoMaterno')?.enable();
    this.formGroup.get('nombres')?.enable();
    //this.formGroup.get('fechaNacimento')?.enable();
    this.formGroup.get('numeroTelefono')?.enable();
    //this.formGroup.get('ciudadTelefono')?.enable();
    //this.formGroup.get('telefono')?.enable();
    this.formGroup.get('departamento')?.enable();
    this.formGroup.get('provincia')?.enable();
    this.formGroup.get('distrito')?.enable();
    this.formGroup.get('domicilioFisico')?.enable();
    // this.formGroup.get('paginaWeb')?.enable();
  }

  
  get f(): { [key: string]: AbstractControl } {
    return this.formGroup.controls ;
  }

  quitarDobleEspacio(idInput: string, e: any) {

    let inicio = this.renderer.selectRootElement(`#${idInput}`).selectionStart;
    let fin = this.renderer.selectRootElement(`#${idInput}`).selectionEnd;
    if (e.metaKey || e.ctrlKey) {
      return true;
    }
    if(inicio == 0 && e.key === ' ') return false;  

    // switch(idInput){
    //   case 'razonSocial':
    //     var value = this.formGroup.get('razonSocial')?.value; 
    //   this.formGroup.get('razonSocial')?.setValue(value.replace(/ {1,}/g, ' ')); break;
    // }
  
    this.renderer.selectRootElement(`#${idInput}`).setSelectionRange(inicio, fin, 'none');

    return true;
  }

  async siguientePaso() {


    if(this.formGroup.valid){
      await this.executeAction('homeLogin');
      var validate = this.formGroup.controls['validateEmail'].value;
      console.log("validate", validate)
      if(!validate){
        this.dialog.open(AlertDialogComponent, {
          disableClose: true,
          hasBackdrop: true,
          data: {cabecera : 'Validación' ,messages: ['No validó el correo electrónico']}
        });
       // this.bloquearValidar = true;
        //this.button.nativeElement.disabled = false;
        return;
      }




      
      var depa = this.formGroup.controls['departamento'].value.nodep;
      var prov = this.formGroup.controls['provincia'].value.noprv;
      var distri = this.formGroup.controls['distrito'].value.nodis;

      this.validateRequestRepresentative.docType = this.formGroup.controls['tipoDocumento'].value;
      this.validateRequestRepresentative.doc = this.formGroup.controls['numeroDocumento'].value;
      this.validateRequestRepresentative.names = this.formGroup.controls['nombres'].value;
      this.validateRequestRepresentative.lastname = this.formGroup.controls['apellidoPaterno'].value;
      this.validateRequestRepresentative.second_lastname = this.formGroup.controls['apellidoMaterno'].value;
      this.validateRequestRepresentative.email = this.formGroup.controls['correoElectronico'].value;
      this.validateRequestRepresentative.cellphone = this.formGroup.controls['numeroCelular'].value;
      this.validateRequestRepresentative.ubigeo = depa + " / " + prov + " / "  + distri;
      this.validateRequestRepresentative.address = this.formGroup.controls['domicilioFisico'].value;
      this.validateRequestRepresentative.position = this.formGroup.controls['cargo'].value.codigo;
      this.validateRequestRepresentative.positionName = this.formGroup.controls['cargo'].value.nombre;
      this.validateRequestRepresentative.documentTypeAttachment = this.formGroup.controls['tipoDocumentoAdjunto'].value;
      this.validateRequestRepresentative.documentNameAttachment = this.formGroup.controls['tipoDocumentoAdjuntoNombre'].value!=""?this.formGroup.controls['tipoDocumentoAdjuntoNombre'].value:this.tipoDocumentoAdjuntoList[this.formGroup.controls['tipoDocumentoAdjunto'].value-1].nombre;
      this.validateRequestRepresentative.recaptcha = this.TOkenCaptcha;
      //this.validateRequestRepresentative.birthday = this.formGroup.controls['fechaNacimento'].value;
      //this.validateRequestRepresentative.verifyCode = this.formGroup.controls['digitoVerificacion'].value;
      this.validateRequestRepresentative.ruc = this.requestSave.numeroDocumento;
    
      this.personaJuridicaService.validarRepresentante(this.validateRequestRepresentative).subscribe(res =>{
        
        if(res.success){
         
          this.loadReaquestSave();
        

        }else{
          this.dialog.open(AlertDialogComponent, {
            disableClose: true,
            hasBackdrop: true,
            data: {cabecera : 'Verifique sus datos' ,messages: [res.message]}
          });
          return;
        }
        
      })




    }else{
      this.formGroup.markAllAsTouched()
      return;
    }

   
  }


  loadReaquestSave(){


         // var nombreCompleto = this.formGroup.controls['nombres'].value + " " +this.formGroup.controls['apellidoPaterno'].value + " " + this.formGroup.controls['apellidoMaterno'].value;
         var cargo = this.formGroup.controls['cargo'].value ;
         var tipoDocumento = this.formGroup.controls['tipoDocumento'].value ;
         var tipoDocumentoAdjunto = this.formGroup.controls['tipoDocumentoAdjunto'].value;

         this.requestRepresentante.documentTypeAttachment = tipoDocumentoAdjunto;
         this.requestRepresentante.documentNameAttachment = this.formGroup.controls['tipoDocumentoAdjuntoNombre'].value!=""?this.formGroup.controls['tipoDocumentoAdjuntoNombre'].value:this.tipoDocumentoAdjuntoList[this.formGroup.controls['tipoDocumentoAdjunto'].value-1].nombre;//this.formGroup.controls['tipoDocumentoAdjuntoNombre'].value;
         this.requestRepresentante.docType = tipoDocumento;
         this.requestRepresentante.doc = this.formGroup.controls['numeroDocumento'].value;
         this.requestRepresentante.names = this.formGroup.controls['nombres'].value;
         this.requestRepresentante.lastname = this.formGroup.controls['apellidoPaterno'].value;
         this.requestRepresentante.second_lastname = this.formGroup.controls['apellidoMaterno'].value;
         this.requestRepresentante.email = this.formGroup.controls['correoElectronico'].value;
         this.requestRepresentante.cellphone = this.formGroup.controls['numeroCelular'].value;
         this.requestRepresentante.address = this.formGroup.controls['domicilioFisico'].value;
         this.requestRepresentante.position = cargo.codigo;
         this.requestRepresentante.positionName = cargo.nombre; // this.formGroup.controls['cargoNombre'].value;
         this.requestRepresentante.phone = this.formGroup.controls['numeroTelefono'].value;
         //this.requestRepresentante.file = this.formGroup.controls['files'].value[0];
   
         var departamento  = this.formGroup.controls['departamento'].value;
         var provincia  = this.formGroup.controls['provincia'].value;
         var distrito  = this.formGroup.controls['distrito'].value;
         let Ubigeo =  departamento.nodep   + " / " + provincia.noprv + " / " + distrito.nodis;
         this.requestRepresentante.ubigeo = Ubigeo;
         this.requestSave.fileDocument = this.formGroup.controls['files'].value[0];
         this.requestSave.representante = this.requestRepresentante;
         this.casillaService.setCasilla(this.requestSave);
   
   
         this.completedStep.emit()

  }

   igualPJ() : boolean{
    const correRep = String(this.formGroup.controls['correoElectronico'].value.trim());
    const correoPJ = String(this.requestSave.correoElectronico);
    if(correRep === correoPJ){
      this.formGroup.get("validateEmail")?.setValue(true);
     // this.formGroup.controls['validateEmail'].setValue(true);
      return true;
    }else{
      this.formGroup.get("validateEmail")?.setValue(false);
     // this.formGroup.controls['validateEmail'].setValue(false);
      return false;
    }
  }

  public filesControl = new FormControl(null, [
    Validators.required,
    FileUploadValidators.accept(['.pdf']),
    FileUploadValidators.filesLimit(1),
    FileUploadValidators.fileSize(1048576 * 10),
   // this.noWhitespaceValidator,
  ]);

  tipoDocumentoCambiado() {

    if(this.cont == 0){
      this.activarInputs();
      //this.formGroup.get('numeroDocumento')?.enable();
    }


 


    const value  = this.formGroup.get('tipoDocumento')?.value;
    const valueTipoDoc = this.formGroup.get('tipoDocumentoAdjunto')?.value;
    const dataEspecifique = this.formGroup.get('tipoDocumentoAdjuntoNombre')?.value;
    this.blockInput = false;
      this.provinciaList = [];
      this.distritoList = [];    
    this.invalidarDocumento();
    
    if (value === TipoDocumento_DNI) {
      this.maxlength = 8;
      this.minlength = 8;
      this.formGroup.get('nombres')?.disable();
      this.formGroup.get('apellidoPaterno')?.disable();
      this.formGroup.get('apellidoMaterno')?.disable();
      this.formGroup.get('numeroDocumento')?.enable();      
      this.formGroup.get('correoElectronico')?.enable();
      if(this.esIos = true){
        this.formGroup.get('numeroDocumento')?.enable();
      }
    } if(value === TipoDocumento_CE) {
      this.maxlength = 9
      this.minlength = 9;
      this.formGroup.get('nombres')?.enable();
      this.formGroup.get('correoElectronico')?.enable();
      this.formGroup.get('apellidoPaterno')?.enable();
      this.formGroup.get('apellidoMaterno')?.enable();
      this.formGroup.get('numeroDocumento')?.enable();
    }

    if(value === "-"){
      this.desactivarInputsInit();
    }
   this.createForm(value,valueTipoDoc,dataEspecifique);

   /*if(value === TipoDocumento_DNI){
    this.formGroup.controls['digitoVerificacion'].setValidators([Validators.required]);
    this.formGroup.controls['digitoVerificacion'].updateValueAndValidity();
    this.formGroup.get("digitoVerificacion")?.reset('');
   }else{
    this.formGroup.controls['digitoVerificacion'].setValidators(null);
    this.formGroup.controls['digitoVerificacion'].updateValueAndValidity();
    this.formGroup.get("digitoVerificacion")?.setValue(" ");
   }*/

  }


  invalidarDocumento() {
    this.numeroDniValido = false
  }
  
  async validarDocumento() {

    if(this.getNumeroDoc().length != this.maxlength) return;

    this.loading = true;
    this.buscando = true;
    this.formGroup.get('numeroDocumento')?.disable();
    console.log('validando documento')
    const numeroDocumento = (this.formGroup.get('numeroDocumento')?.value ?? '') as string
    if (this.esTipoDocumentoDni  && numeroDocumento.length == 8) {
      
      var validate = await this.executeAction('homeLogin'); //  poner en true para desarrollo
      
      if(validate){
        let envio : ObtenerDatosPersonaDniDto = new ObtenerDatosPersonaDniDto();
        envio.dni = numeroDocumento;
        envio.recaptcha = this.TOkenCaptcha;
        //this.personaNaturalDni = await firstValueFrom(this.personaNaturalService.obtenerDatosPersona(envio))

        this.personaNaturalService.obtenerDatosPersona(envio).subscribe(res =>{
          if(res){
            this.personaNaturalDni = res;
            this.formGroup.patchValue({
              'nombres': this.personaNaturalDni.nombres.trimRight(),
              //'apellidos': this.personaNaturalDni.apellidos,
              'apellidoPaterno': this.personaNaturalDni.apellidoPaterno.trimRight(),
              'apellidoMaterno': this.personaNaturalDni.apellidoMaterno.trimRight(),
            });
            this.loading = false;
            this.buscando = false;
          this.blockInput = false;          
          this.numeroDniValido = true;
          }else{
            this.blockInput = true;
            this.loading = false;
            this.formGroup.get('numeroDocumento')?.enable();
            this.formGroup.get('tipoDocumento')?.enable();
            this.dialog.open(AlertDialogComponent, {
              disableClose: true,
              hasBackdrop: true,
              data: {cabecera : 'Error' ,messages: ['No hubo respuesta, intente nuevamente en unos momentos.']}
            });
            return;
          }

        },error =>{
          let mensajeError = {cabecera : 'Advertencia', messages: ['Error al obtener información.']};
          if(error.error.statusCode == 401){
            mensajeError = {cabecera : 'No autorizado', messages: [error.error.message]};
          }
          if(error.error.statusCode == 404){
            mensajeError = {cabecera : 'Verifica si el número de DNI ingresado es correcto.', messages: ['En caso sea correcto, te invitamos a presentar tu Solicitud mediante Mesa de Partes Física o Virtual.']};
          }
          this.blockInput = true;
          this.loading = false;
          this.formGroup.get('numeroDocumento')?.enable();
          this.formGroup.get('tipoDocumento')?.enable();
          this.loading = false;
          this.buscando = false;
          this.dialog.open(AlertDialogComponent, {
            disableClose: true,
            hasBackdrop: true,
            data: mensajeError
          });
          return;
        })
     
      }else{
        this.dialog.open(AlertDialogComponent, {
          disableClose: true,
          hasBackdrop: true,
          data: {cabecera : 'Error' ,messages: ['No hubo respuesta, intente nuevamente en unos momentos.']}
        });
        this.loading = false;
        this.buscando = false;
        return;
      }
 
    }
  }

  obtenerCorreo() {
    return this.formGroup.get('correoElectronico')?.value ?? ''
  }

  getNumeroDoc(){
    return this.formGroup.get('numeroDocumento')?.value ?? '';
  }

  get esTipoDocumentoDni() {
    return this.formGroup?.get('tipoDocumento')?.value == TipoDocumento_DNI
  }

  async continuar() {
    if (await this.validarCorreoService.iniciarValidacion(this.obtenerCorreo(), this.codigoEnviado)) {
      console.log('validación correcta')
      this.siguientePaso()
    } else {
      console.log('no se ha validado')
      this.codigoEnviado = true
    }
  }

  
  async cambiarProvincia() {
    this.formGroup.get("provincia")?.reset("");
    this.formGroup.get("distrito")?.reset("");
    this.provinciaList = [];  
    var value  = this.formGroup.get('departamento')?.value.ubdep
    this.provinciaList = await firstValueFrom(this.ubigeoService.getProvinciaList(value))
    this.distritoList = []
    
  }

  async cambiarDistrito() {
    this.distritoList = [];
    this.formGroup.get("distrito")?.reset("");
    var valueprovincia = this.formGroup.get('provincia')?.value.ubprv
    var valuedepar = this.formGroup.get('departamento')?.value.ubdep
    this.distritoList = await firstValueFrom(this.ubigeoService.getDistritoList(valuedepar, valueprovincia))
  }

  validardomicilio(e : any, idInput: string){
    var value = this.formGroup.get('domicilioFisico')?.value;

    let inicio = this.renderer.selectRootElement(`#${idInput}`).selectionStart;
    let fin = this.renderer.selectRootElement(`#${idInput}`).selectionEnd;
    if (e.metaKey || e.ctrlKey) {
      return true;
    }
    if(inicio == 0 && e.key === ' ') return false;
    
    this.formGroup.get('domicilioFisico')?.setValue(value.replace(/ {2,}/g, ' '));
    this.renderer.selectRootElement(`#${idInput}`).setSelectionRange(inicio, fin, 'none');
      return true;
   }

  validarCelular(event : any): boolean{
    const charCode = (event.which) ? event.which : event.keyCode;
    const numCelular = this.formGroup.get('numeroCelular')?.value;
    var posicion = event.target.selectionStart;
    var primerdato = numCelular[0];
    if(numCelular != ""){
      if(primerdato != 9  && charCode != 57)
        return false;
    }
    if(posicion == 0 ){
      if(charCode == 57 ){
        return true;
      }else{
        return false;
      }
    }else{
      if( charCode > 31 && (charCode < 48 || charCode > 57)){
        return false;
      }else {

        if(numCelular != ""){
          if(primerdato != 9  )
            return false;
        }else{
          return true;
        }

        return true;
      }
    }
   }

   validarsoloTelefonoFijo(event : any): boolean{
    const charCode = (event.which) ? event.which : event.keyCode;
    const digitVer = this.formGroup.get('numeroTelefono')?.value;
    if(digitVer == " "){
      this.formGroup.get("numeroTelefono")?.setValue("");
    }
    var inp = String.fromCharCode(event.keyCode);
    if (charCode===45){return true;}
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
   }


  async validarCorreoElectronico() {
    this.oneClick = true;
    var validate = await this.executeAction('homeLogin'); 

     var igualPj = this.igualPJ();

     if(igualPj){
     this.dialog.open(AlertDialogComponent, {
        disableClose: true,
        hasBackdrop: true,
        data: {cabecera : 'Validado' ,messages: ['El correo ya fue validado']}
      });
      this.formGroup.get('correoElectronico')?.disable();
      this.oneClick = false;
      return;
     }

    let request = {
      tipoDocumento : this.formGroup.get('tipoDocumento')?.value ,
      numeroDocumento : this.formGroup.get('numeroDocumento')?.value,
      correoElectronico : this.formGroup.get('correoElectronico')?.value,
      recaptcha : this.TOkenCaptcha
      }

    this.correoService.envioCorreoVerificacion(request).subscribe(res =>{
      this.oneClick = false;
      if(res){
        Object.assign(request, {personType : 'pj'} )

        const dialogRef = this.dialog.open(SharedDialogComponent, {
          width: "771px",
          height : "434px",  
          disableClose: true,
          data: {  idEnvio :res.idEnvio , requestData : request , email : this.formGroup.get('correoElectronico')?.value},
        });
        dialogRef.afterClosed().subscribe((result : boolean) => {
          this.formGroup.get("validateEmail")?.setValue(result);
          //this.formGroup.controls['validateEmail'].setValue(result);
         // this.formGroup.patchValue({'validateEmail': result});
          // this.formGroup.setValue({validateEmail : result})
          //this.formGroup.controls.validateEmail.setValue(result);
          if(result){
            this.formGroup.get('correoElectronico')?.disable();
          }
        });        
      }else{
        this.dialog.open(AlertDialogComponent, {
          disableClose: true,
          hasBackdrop: true,
          data: {cabecera : 'Error!' ,messages: ['Error al  registrar']}
        })
      }
    }, error => {
      this.oneClick = false;
    });
  }

  validarsoloNumeros(event : any): boolean{
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
   }

   
  
  ActiveButton():boolean{

    const validateEmail = this.formGroup.controls['validateEmail'].value;
    //console.log("validateEMAILLLLL" , validateEmail);
    if( validateEmail == true){
      return true;
    }else{
      if( this.formGroup.get('tipoDocumento')?.invalid ||this.formGroup.get('numeroDocumento')?.invalid || this.formGroup.get('correoElectronico')?.invalid || this.formGroup.get('tipoDocumento')?.value == "-"){
        return true
      }
      else{
        return false;
      }
    }
  }

   
   public recentToken = '';
   public recentError?: { error: any };
   private singleExecutionSubscription!: Subscription;
   private executeAction = async (action: string) => {
    return new Promise((resolve) => {
      if (this.singleExecutionSubscription) {
        this.singleExecutionSubscription.unsubscribe();
      }
      this.singleExecutionSubscription = this.reCaptchaV3Service
        .execute(action)
        .subscribe(
          (token) => {
            this.recentToken = token;
            this.recentError = undefined;
            this.TOkenCaptcha = token;console.log("Tocken persona-natural: "+this.TOkenCaptcha);
            this.formGroup.get("recaptchaReactive")?.setValue(this.TOkenCaptcha);
            resolve(true);
          },
          (error) => {
            this.recentToken = '';
            this.TOkenCaptcha = '';
            this.recentError = { error };
            resolve(false);
          }
        );
    });
  };




  validateForms():boolean{

    if(this.formGroup.valid ){
      return false;
  
    }else{
      return true;
    }


  }


}

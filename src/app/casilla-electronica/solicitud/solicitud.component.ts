import { DOCUMENT } from '@angular/common';
import {Component, EventEmitter, HostBinding, Inject, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { MatDialog } from '@angular/material/dialog';
import {  Router } from '@angular/router';
import { ReCaptchaV3Service } from 'ng-recaptcha';
import { Subscription } from 'rxjs';
import { Condicion_Persona_Juridica } from 'src/app/core/dto/documento';
import { requestGlobal, RequestRepresentante } from 'src/app/core/dto/request';
import { CasillaService } from 'src/app/core/services/casilla.service';
import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';

export const EndPontPersonaNatural = '/create-box';
export const EndPontPersonaJuridica = '/legal/inbox/create-box';


@Component({
  selector: 'app-solicitud',
  templateUrl: './solicitud.component.html',
  styleUrls: ['./solicitud.component.css']
})
export class SolicitudComponent implements OnInit {

 
 

  @Output() completedStep = new EventEmitter<any>()
  @Output() previousStep = new EventEmitter<any>()
  formGroup!: FormGroup;
  listFiles : File[] = [];
  imageSrc: string="";
  deshabilitado : boolean = false;
  observableRequestSubscription!: Subscription;
  requestSave: requestGlobal = new requestGlobal();
  requestRepresentante : RequestRepresentante = new RequestRepresentante();
  TOkenCaptcha: string = '';

  constructor(
    private formBuilder: FormBuilder,
    private casillaService: CasillaService,
    public dialog: MatDialog,
    private router : Router,
    private reCaptchaV3Service: ReCaptchaV3Service,
    @Inject(DOCUMENT) private document: Document
  ) {
    this.observableRequestSubscription = casillaService.casilla$.subscribe(
      (requestSave: requestGlobal) => {
        this.requestSave = requestSave;
        this.requestRepresentante = this.requestSave!.representante;
        console.log("data enviar", this.requestSave)
    
        this.onFileChange(this.requestSave.file);
      }
    );
  }



  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      name: ['', Validators.required]
    });


  }

  continuar() {
    this.deshabilitado = true;
    //this.completedStep.emit(true)
    this.dialog.open(AlertDialogComponent, {
      disableClose: true,
      hasBackdrop: true,
      data: {cabecera : 'Notificación' ,messages: ['¿Está de acuerdo con enviar la información?'],btnCancel : true}
    }).afterClosed().subscribe(result =>{
     if(result){
      this.enviar();
     }else{
      this.deshabilitado = false;
     }
    
    });

  }

  regresar() {
    this.previousStep.emit()
  }




async  enviar(){

    var validate = await this.executeAction('homeLogin'); 

   // if(!validate) return;
  var CreateBoxTipoPersona  ="";

    const fd = new FormData();

    if(this.requestSave.TipoPersona === Condicion_Persona_Juridica){
      fd.append('docType',this.requestSave.tipoDocumento)
      fd.append('doc',this.requestSave.numeroDocumento)
      fd.append('organizationName',this.requestSave.razonSocial)
      fd.append('email',this.requestSave.correoElectronico)
      fd.append('cellphone',this.requestSave.numeroCelular)
      fd.append('phone',this.requestSave.telefono)
      let Ubigeo = this.requestSave.departamento + " / " +this.requestSave.provincia + " / " + this.requestSave.distrito
      fd.append('ubigeo',Ubigeo)  
      fd.append('address',this.requestSave.domicilioFisico)
      fd.append('webSite',this.requestSave.paginaWeb)
      fd.append('recaptcha', this.TOkenCaptcha)
      fd.append('rep',JSON.stringify(this.requestSave.representante))
      fd.append('fileDocument',this.requestSave.fileDocument)
      fd.append('filePhoto',this.requestSave.file)
      fd.append('personType','pj')
      this.listFiles.push(this.requestSave.fileDocument);
      CreateBoxTipoPersona = EndPontPersonaJuridica;


     
    }else{
      fd.append('docType',this.requestSave.tipoDocumento)
      fd.append('doc',this.requestSave.numeroDocumento)
      fd.append('name',this.requestSave.nombres)
      fd.append('lastname',this.requestSave.apePaterno)
      fd.append('second_lastname',this.requestSave.apeMaterno)
      fd.append('email',this.requestSave.correoElectronico)
      fd.append('cellphone',this.requestSave.numeroCelular)
      fd.append('phone',this.requestSave.telefono)
      let Ubigeo = this.requestSave.departamento + " / " +this.requestSave.provincia + " / " + this.requestSave.distrito
      fd.append('ubigeo',Ubigeo) 
      fd.append('address',this.requestSave.domicilioFisico)
      fd.append('recaptcha', this.TOkenCaptcha)
      fd.append('filePhoto',this.requestSave.file)
      fd.append('personType','pn')
      CreateBoxTipoPersona = EndPontPersonaNatural;

    }

    this.casillaService.enviarDatos(fd,CreateBoxTipoPersona).subscribe(res =>{
      if(res.success){
        // this.dialog.open(AlertDialogComponent, {
        //   disableClose: true,
        //   hasBackdrop: true,
        //   data: {cabecera : '¡Solicitud Enviada!' ,messages: ['Se ha enviado tu solicitud de registro de casilla.']}
        // }).afterClosed().subscribe(result =>{
        //   this.document.location.href = 'https://casillaelectronica.onpe.gob.pe/#/login';
        // });
        this.completedStep.emit();
        
      }else{
        this.dialog.open(AlertDialogComponent, {
          disableClose: true,
          hasBackdrop: true,
          data: {cabecera : '¡Advertencia!' ,messages: [res.message]}
        })
       
      }

      this.deshabilitado = false;

    },error =>{
      this.dialog.open(AlertDialogComponent, {
        disableClose: true,
        hasBackdrop: true,
        data: {cabecera : '¡Advertencia!' ,messages: ['Error de servicio.']}
      })
      this.deshabilitado = false;
    });

  }

  onFileChange(foto : File) {
    const reader = new FileReader();    
    if(foto) {
      const file = foto;
      reader.readAsDataURL(file);
    
      reader.onload = () => {
   
        this.imageSrc = reader.result as string;     
        // this.myForm.patchValue({
        //   fileSource: reader.result
        // });   
      };   
    }
  }


  downloadPDF = async ( ) => {
    try {
     
      const file = this.requestSave.fileDocument

      console.log('archivoo', file);
      this.casillaService.downloadFile(file, file.name);
 
    } catch (error) {
   
    }
  };

  
  public recentToken = '';
  public recentError?: { error: any };
  private singleExecutionSubscription!: Subscription;
  private executeAction = async (action: string) => {
   return new Promise((resolve) => {
     if (this.singleExecutionSubscription) {
       this.singleExecutionSubscription.unsubscribe();
     }
     this.singleExecutionSubscription = this.reCaptchaV3Service
       .execute(action)
       .subscribe(
         (token) => {
           this.recentToken = token;
           this.recentError = undefined;
           this.TOkenCaptcha = token;console.log("Tocken solicitud: "+this.TOkenCaptcha);
           //this.formGroup.get("recaptchaReactive")?.setValue(this.TOkenCaptcha);
           resolve(true);
         },
         (error) => {
           this.recentToken = '';
           this.TOkenCaptcha = '';
           this.recentError = { error };
           resolve(false);
         }
       );
   });
  };




}
